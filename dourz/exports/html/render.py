import copy

import requests

from dourz.douban.dml import DoubanMarkupParser
from dourz.exports.jinja2 import Jinja2
from dourz.utils import sha1


class BookRender(object):
    """The render to render a book into html."""

    def __init__(self, book):
        self.jinja2 = Jinja2(__package__, 'templates')
        self.jinja2.environment.globals['preferred_image_size'] = \
            OfflineImage.preferred_size
        self.book = book
        self.dml = DoubanMarkupParser()

    def render_chapter(self, chapter):
        contents = self.parse_dml(chapter['contents'])
        return self.jinja2.render_template(
            'chapter.xhtml', chapter=chapter, contents=contents, dml=self.dml)

    def render_chapter_with_index(self, index):
        return self.render_chapter(self.chapters[index])

    def render_chapters(self):
        for chapter in self.chapters:
            yield self.render_chapter(chapter)

    @property
    def inline_images(self):
        return self.dml.images.values()

    @property
    def chapters(self):
        return self.book['posts']

    def parse_dml(self, contents):
        """Parse the chapter's contents with the douban markup language."""
        contents = copy.deepcopy(contents)
        for content in contents:
            data = content.get('data', {})
            if 'text' in data:
                data['text'] = self.dml.parse(data['text'])
        return contents


class OfflineImage(object):
    """The information of image which will be downloaded as offline file."""

    sizes = ['large', 'medium', 'orig', 'small', 'tiny']

    def __init__(self, data, choosen_size=None):
        self.choosen_size = choosen_size or self.preferred_size(data)
        if self.choosen_size not in self.sizes:
            raise ValueError("%r not in %r" % (choosen_size, self.sizes))
        self.data = data
        self.cached_bytes = {}

    @property
    def image(self):
        return self.data['size'][self.choosen_size]

    @property
    def filename(self):
        return sha1(self.image['src'])

    @property
    def image_filename(self):
        return self.filename + '.jpg'

    @property
    def image_filepath(self):
        return 'Content/Image/' + self.image_filename

    @property
    def image_bytes(self):
        size = self.choosen_size
        if size not in self.cached_bytes:
            self.cached_bytes[size] = download_image(self.image['src'])
        return self.cached_bytes[size]

    @classmethod
    def preferred_size(cls, data):
        for size in cls.sizes:
            if size in data['size']:
                return size
        raise ValueError('not available size: %r' % data)


image_session = requests.Session()
image_session.mount('http://', requests.adapters.HTTPAdapter(max_retries=3))
image_session.mount('https://', requests.adapters.HTTPAdapter(max_retries=3))


def download_image(url):
    response = image_session.get(url, timeout=60)
    response.raise_for_status()
    return response.content
