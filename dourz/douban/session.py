import collections
import random

import requests
import lxml.html
import wand.image
from requests.utils import dict_from_cookiejar, cookiejar_from_dict
from six.moves.urllib.parse import urlparse

from . import consts


CaptchaSolution = collections.namedtuple('CaptchaSolution', ['id', 'solution'])


class CaptchaImage(collections.namedtuple('CaptchaImage', ['id', 'bytes'])):
    """The captcha image."""

    def make_image(self):
        return wand.image.Image(blob=self.bytes)


class DoubanSession(object):
    """The login session for douban account."""

    def __init__(self):
        self.requests = requests.Session()
        self.requests.headers['User-Agent'] = \
            random.choice(consts.BROWSER_USERAGENTS)
        self._has_login = False

    def load_cookies(self, cookies):
        self.requests.cookies = cookiejar_from_dict(cookies)

    def dump_cookies(self):
        return dict_from_cookiejar(self.requests.cookies)

    @property
    def has_login(self):
        """Check current session is authencated or not."""
        if self._has_login:
            return True
        if self.fetch_uname():
            self._has_login = True
        return self._has_login

    def fetch_uname(self):
        """Fetch user's douban id."""
        response = self.requests.get(consts.MINE_URL)
        response.raise_for_status()
        mine_url = urlparse(response.url)
        if mine_url.path.startswith('/people/'):
            return mine_url.path[len('/people/'):].strip('/')
        if mine_url.path.startswith('/accounts/login'):
            raise RequireLoginError
        raise RuntimeError('unknown response')

    def logout(self, clear_all=False):
        if clear_all:
            self.requests.cookies.clear()
        else:
            for key in ['ck', 'ue', 'dbcl2']:
                self.requests.cookies.pop(key, None)

    def login(self, username, password, captcha_solution=None):
        """Login a douban account.

        :param username: the douban login identity.
        :param password: the plain text password of current account.
        """
        data = {'source': 'index_nav',
                'redir': consts.HOME_URL,
                'form_email': username,
                'form_password': password[:20],
                'user_login': consts.LOGIN_BTN_TEXT}
        if captcha_solution:
            data['source'] = 'simple'
            data['captcha-id'] = captcha_solution.id
            data['captcha-solution'] = captcha_solution.solution

        response = self.requests.post(consts.LOGIN_URL, data=data)
        etree = lxml.html.fromstring(response.text)

        # errors occured
        errors = etree.xpath('//div[@id="item-error"]/p/text()')
        if errors:
            import pdb
            pdb.set_trace()
            raise LoginError(', '.join(errors))

        # require to provide captcha
        captcha_url = ''.join(etree.xpath('//img[@id="captcha_image"]/@src'))
        captcha_id = ''.join(etree.xpath('//input[@name="captcha-id"]/@value'))
        if captcha_url and captcha_id:
            captcha_bytes = self.requests.get(captcha_url).content
            captcha = CaptchaImage(captcha_id, captcha_bytes)
            raise RequireCaptcha(captcha=captcha, username=username,
                                 password=password)

    @property
    def ck(self):
        """The CSRF token."""
        return self.requests.cookies.get('ck').strip('"')


class DoubanSessionMixin(object):
    """The collection of shortcuts for douban session related classes."""

    @property
    def has_login(self):
        return self.session.has_login

    @property
    def requests(self):
        if not self.has_login:
            raise RequireLoginError
        return self.session.requests

    @property
    def ck(self):
        if not self.has_login:
            raise RequireLoginError
        return self.session.ck


def session_related(factory_method_name):
    """Register a class as session related.

    Example::

        @session_related('frodo')
        class DoubanFrodo(object):
            def __init__(self, session, **kwargs):
                self.session = session
                self.kwargs = kwargs

        session = DoubanSession()
        session.frodo(foo=1)  # equals to `DoubanFrodo(session, foo=1)`
    """
    def decorator(cls):
        # make shortcut method
        def fn(self, *args, **kwargs):
            return cls(self, *args, **kwargs)
        fn.__name__ = fn.func_name = factory_method_name
        # assign to douban session class
        setattr(DoubanSession, factory_method_name, fn)
        return cls
    return decorator


class DoubanLoginException(Exception):
    """The login has been interrupted."""


class RequireCaptcha(DoubanLoginException):
    """Failed to login douban because the captcha is required."""

    def __init__(self, message=None, captcha=None, username=None,
                 password=None):
        super(RequireCaptcha, self).__init__(message or '')
        self.captcha = captcha
        self.username = username
        self.password = password


class LoginError(DoubanLoginException):
    """Some errors occured while login."""


class RequireLoginError(DoubanLoginException):
    """Require to login first."""
