import json

from .session import DoubanSessionMixin, session_related
from .encryption import douban_hex64
from .consts import READER_BOOK_URL, READER_BOOKSHELF_URL, READER_DATA_URL
from ..utils import camelcase_to_underscore


@session_related('reader')
class DoubanReader(DoubanSessionMixin):
    """The read.douban.com support."""

    def __init__(self, session):
        self.session = session

    def bookshelf(self):
        response = self.requests.get(READER_BOOKSHELF_URL)
        response.raise_for_status()
        books = response.json()
        return [Book.from_json(self.session, book) for book in books]


class Book(DoubanSessionMixin):
    """The book entity."""

    hex64 = douban_hex64()

    def __init__(self, session, **kwargs):
        self.session = session
        vars(self).update(kwargs)

    @classmethod
    def from_json(cls, session, book):
        kwargs = {camelcase_to_underscore(k): v for k, v in book.items()}
        return cls(session, **kwargs)

    def raw_data(self):
        data = {'aid': self.id, 'reader_data_version': 'v10'}
        headers = {'X-CSRF-Token': self.ck,
                   'X-Requested-With': 'XMLHttpRequest',
                   'Referer': READER_BOOK_URL.format(aid=self.id)}
        response = self.requests.post(
            READER_DATA_URL, data=data, headers=headers)
        response.raise_for_status()
        return response.json()

    def decrypt_data(self):
        raw_data = self.raw_data()
        if raw_data and 'data' in raw_data:
            return self.hex64.decode(raw_data['data'])
        raise ValueError(raw_data)

    def data_as_dict(self):
        plain_data = self.decrypt_data()
        return json.loads(plain_data)
