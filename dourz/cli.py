import os
import json
import errno
import tempfile
import collections
import zipfile
import subprocess
import select
from distutils.spawn import find_executable

import click

from . import __version__ as dourz_version
from .douban import (
    DoubanSession, RequireLoginError, RequireCaptcha, CaptchaSolution)
from .exports.epub.render import BookContext


class Workspace(object):
    def __init__(self, workdir=None, debug=False):
        self.workdir = os.path.expanduser(workdir or '~/.dourz')
        self.session = DoubanSession()
        self.debug = debug

    def open_resource(self, filename, *args, **kwargs):
        full_location = os.path.join(self.workdir, filename)
        full_parent = os.path.dirname(full_location)
        try:
            os.makedirs(full_parent)
        except OSError as e:
            if e.errno != errno.EEXIST or not os.path.isdir(full_parent):
                raise
        return open(full_location, *args, **kwargs)

    def resource_exists(self, filename):
        full_location = os.path.join(self.workdir, filename)
        return os.path.exists(full_location)

    def load_cookies(self):
        if not self.resource_exists('login-session'):
            return
        with self.open_resource('login-session', 'r') as session_file:
            try:
                cookies = json.load(session_file)
            except ValueError:
                return  # TODO warning here
        self.session.load_cookies(cookies)

    def dump_cookies(self):
        cookies = self.session.dump_cookies()
        with self.open_resource('login-session', 'w') as session_file:
            json.dump(cookies, session_file)

    def show_status(self):
        try:
            uname = self.session.fetch_uname()
        except RequireLoginError:
            click.secho('unauthorized', err=True, fg='red')
        else:
            click.secho('authorized: {0}'.format(uname), fg='green')

    def login(self, username, password, captcha_id=None, solution=None):
        if captcha_id and solution:
            solution = CaptchaSolution(captcha_id, solution)
        else:
            solution = None
        self.session.login(username, password, solution)
        self.dump_cookies()

    def logout(self, clear_all=False):
        self.session.logout(clear_all=clear_all)
        self.dump_cookies()

    def list_books(self):
        reader = self.session.reader()
        books = collections.OrderedDict(
            (int(book.id), book) for book in reader.bookshelf())
        return books

    def export_book(self, book_id, output_format):
        book = self.list_books()[book_id]
        book_context = BookContext(book)
        book_title = book.title.strip()

        click.secho(
            u'{0}:{1}'.format(book_id, book_title), bg='green', fg='white')
        click.secho(u'* fetching and rendering', fg='green')
        book_content = list(book_context.render_all())

        if output_format in ('epub', 'mobi'):
            epub_filename = u'%s.epub' % book.title.strip()
            click.secho(
                u'* writting into {0}'.format(epub_filename), fg='green')
            with zipfile.ZipFile(epub_filename, 'w') as book_epub:
                for filename, content in book_content:
                    book_epub.writestr(filename, content)
            if output_format == 'mobi':
                click.secho(u'* converting into mobi', fg='green')
                self.execute_external_process(['kindlegen', epub_filename])
        else:
            click.fail(u'unknown format: {0}'.format(output_format))
        click.secho(u'* done', fg='green')

    def execute_external_process(self, args):
        if not find_executable(args[0]):
            raise RuntimeError('{0} is not found in PATH'.format(args[0]))

        proc = subprocess.Popen(
            args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        while True:
            fds, _, _ = select.select(
                [proc.stdout.fileno(), proc.stderr.fileno()], [], [])
            for fd in fds:
                line = None
                if fd == proc.stdout.fileno():
                    line = proc.stdout.readline().strip()
                    color = 'black'
                if fd == proc.stderr.fileno():
                    line = proc.stderr.readline().strip()
                    color = 'red'
                if line:
                    click.secho(' | ' + line, fg=color)
            if proc.poll() is not None:
                return proc.poll()


@click.group()
@click.option('--workdir', type=click.Path(file_okay=False), default=None,
              envvar='DOURZ_WORKDIR')
@click.option('--debug/--no-debug', default=False, envvar='DOURZ_DEBUG')
@click.pass_context
def cli(ctx, debug, workdir):
    ctx.obj = Workspace(debug=debug, workdir=workdir)
    ctx.obj.load_cookies()


@cli.command()
@click.pass_obj
def status(workspace):
    """Shows current authorized user."""
    workspace.show_status()


@cli.command()
@click.argument('username')
@click.option('--captcha-id', default='')
@click.option('--captcha-solution', default='')
@click.pass_obj
def login(workspace, username, captcha_id, captcha_solution):
    """Login a Douban account."""
    password = click.prompt('password', hide_input=True)
    try:
        workspace.login(username, password, captcha_id, captcha_solution)
    except RequireCaptcha as e:
        _, image_filename = tempfile.mkstemp(suffix='.jpg')
        with e.captcha.make_image() as image:
            image.save(filename=image_filename)
        click.launch(image_filename)
        click.echo('captcha is located in {0}'.format(image_filename))
        click.echo('please try again with captcha solution:')
        click.echo(
            ' dourz login {0} --captcha-id={1} --captcha-solution='.format(
                username, e.captcha.id))


@cli.command()
@click.option('--clear-all', default=False, is_flag=True)
@click.pass_obj
def logout(workspace, clear_all):
    """Logout current Douban account."""
    workspace.logout(clear_all=clear_all)
    click.secho('done', err=True, fg='green')


@cli.command()
@click.pass_obj
def books(workspace):
    """Lists all books."""
    for book_id, book in workspace.list_books().items():
        click.echo(u'{0}\t{1}'.format(book_id, book.title))


@cli.command()
@click.option('--format', default='epub', type=click.Choice(['epub', 'mobi']))
@click.argument('id', type=int)
@click.pass_obj
def export(workspace, id, format):
    """Exports a book by its id."""
    workspace.export_book(id, format)


@cli.command()
def version():
    """Shows current version."""
    click.echo(dourz_version)


if __name__ == '__main__':
    cli()
