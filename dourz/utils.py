import re
import hashlib
import collections

import matplotlib.pyplot as plt
from six import BytesIO


def camelcase_to_underscore(name):
    """
    >>> camelcase_to_underscore('translatorName')
    'translator_name'
    >>> camelcase_to_underscore('defaultSharingText')
    'default_sharing_text'
    >>> camelcase_to_underscore('title')
    'title'
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def sha1(raw_string):
    """
    >>> sha1(u'x')
    '11f6ad8ec52a2984abaafd7c3b516503785c2072'
    >>> sha1(b'x')
    '11f6ad8ec52a2984abaafd7c3b516503785c2072'
    """
    raw_string = safe_to_bytes(raw_string)
    hashobject = hashlib.sha1(raw_string)
    return hashobject.hexdigest()


def safe_to_unicode(text, encoding='utf-8'):
    """
    >>> safe_to_unicode(b'\\xe8\\x8b\\xb9\\xe6\\x9e\\x9c') == u'\\u82f9\\u679c'
    True
    >>> safe_to_unicode(u'\\u82f9\\u679c') == u'\\u82f9\\u679c'
    True
    """
    if isinstance(text, bytes):
        return text.decode(encoding)
    return text


def safe_to_bytes(text, encoding='utf-8'):
    """
    >>> safe_to_bytes(b'\\xe8\\x8b\\xb9\\xe6\\x9e\\x9c') == \\
    ...     b'\\xe8\\x8b\\xb9\\xe6\\x9e\\x9c'
    True
    >>> safe_to_bytes(u'\\u82f9\\u679c') == b'\\xe8\\x8b\\xb9\\xe6\\x9e\\x9c'
    True
    """
    if isinstance(text, bytes):
        return text
    return text.encode(encoding)


def render_latex(formula, fontsize=12, dpi=300, format_='svg'):
    """Renders LaTeX formula into image.

    Thanks for http://stackoverflow.com/a/14163131/718453
    """
    fig = plt.figure()
    text = fig.text(0, 0, u'${0}$'.format(formula), fontsize=fontsize)

    fig.savefig(BytesIO(), dpi=dpi)  # triggers rendering

    bbox = text.get_window_extent()
    width, height = bbox.size / float(dpi) + 0.05
    fig.set_size_inches((width, height))

    dy = (bbox.ymin / float(dpi)) / height
    text.set_position((0, -dy))

    buffer_ = BytesIO()
    fig.savefig(buffer_, dpi=dpi, transparent=True, format=format_)
    plt.close(fig)
    buffer_.seek(0)

    return buffer_


def without_duplicate_items(iterable, identity_key):
    return collections.OrderedDict(
        (getattr(item, identity_key), item) for item in iterable).values()
