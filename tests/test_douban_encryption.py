import json

from py.path import local
from pytest import fixture

from dourz.douban.encryption import douban_hex64


@fixture
def book_data_raw():
    return local(__file__).dirpath('data/book_data.txt').read()


@fixture
def book_data_json():
    book_json = local(__file__).dirpath('data/book_data.json').read()
    return json.loads(book_json)


@fixture
def hex64():
    return douban_hex64()


def test_decoding(book_data_raw, book_data_json, hex64):
    book = json.loads(hex64.decode(book_data_raw))
    assert book == book_data_json

    assert set(book) == {
        'abstract',
        'authorId',
        'hasFormula',
        'pages',
        'posts',
        'price',
        'sample',
        'toc'}
    assert book['pages'] == 26
    assert len(book['posts']) == 2

    chapter = book['posts'][0]
    assert set(chapter) == {
        'contents',
        'orig_author',
        'orig_title',
        'subtitle',
        'title',
        'translator'}
    assert len(chapter['contents']) == 52

    paragraph = chapter['contents'][42]
    assert set(paragraph) == {'data', 'id', 'type'}
    assert paragraph['id'] == 777429
    assert paragraph['type'] == 'paragraph'

    data = paragraph['data']
    assert set(data) == {'format', 'text'}
    assert data['format'] == {
        'p_align': u'left',
        'p_center': False,
        't_indent': False,
        'p_indent': False,
        'p_quote': False,
        'p_bold': False}
    assert len(data['text']) == 18
