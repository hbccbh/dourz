from pytest import fixture

from dourz.exports.jinja2 import Jinja2


@fixture
def render_template():
    return Jinja2(__package__, 'data').render_template


def test_jinja2(render_template):
    html = render_template('template.html', name='world')
    assert html == '<p>hello, world</p>'


def test_filters(render_template):
    lines = render_template('template_filters.txt').split('\n')

    assert lines[0] == '2012-11-07 15:24:03'
    assert lines[1] == 'a9993e364706816aba3e25717850c26c9cd0d89d'
